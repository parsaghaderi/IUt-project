#ifndef TARGET_H
#define TARGET_H
#include<QObject>
#include<QGraphicsRectItem>
#include<QTimer>
#include<QGraphicsPixmapItem>
#include<QPixmap>
class target:public QObject ,public QGraphicsPixmapItem
{
public:
    target();
public slots:
virtual void move()=0;
};

#endif // TARGET_H
